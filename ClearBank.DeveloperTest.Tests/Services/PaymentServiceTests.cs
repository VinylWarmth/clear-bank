using System;
using ClearBank.DeveloperTest.Data;
using ClearBank.DeveloperTest.Services;
using ClearBank.DeveloperTest.Types;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NSubstitute.ReturnsExtensions;
using NUnit.Framework;
using Shouldly;

namespace ClearBank.DeveloperTest.Tests.Services;

public class PaymentServiceTests
{
    private IAccountDataStore _accountDataStore;
    private PaymentService _sut;

    [SetUp]
    public void Setup()
    {
        _accountDataStore = Substitute.For<IAccountDataStore>();
        _sut = new PaymentService(_accountDataStore, new PaymentRequestValidator());
    }
    
    [Test]
    public void MakePayment_ShouldNotUpdateAccount_WhenAccountDoesNotExist()
    {
        _accountDataStore.GetAccount(Arg.Any<string>()).ReturnsNull();

        var result = _sut.MakePayment(new MakePaymentRequest());
        
        _accountDataStore.Received(0).UpdateAccount(Arg.Any<Account>());
        result.Success.ShouldBeFalse();
    }
    
    [Test]
    [TestCase(AllowedPaymentSchemes.Chaps)]
    [TestCase(AllowedPaymentSchemes.FasterPayments)]
    public void MakePayment_ShouldNotUpdateAccount_WhenRequestPaymentSchemeIsBacsAndAccountAllowedPaymentSchemesDoesNotIncludeBacs(AllowedPaymentSchemes allowedPaymentScheme)
    {
        _accountDataStore.GetAccount(Arg.Any<string>()).Returns(new Account{AllowedPaymentSchemes = allowedPaymentScheme });

        var result = _sut.MakePayment(new MakePaymentRequest{PaymentScheme = PaymentScheme.Bacs});
        
        _accountDataStore.Received(0).UpdateAccount(Arg.Any<Account>());
        result.Success.ShouldBeFalse();
    }
    
    [Test]
    public void MakePayment_ShouldUpdateAccount_WhenRequestPaymentSchemeIsBacsAndAccountAllowedPaymentSchemesIsBacsAndBalanceIsGreaterThanRequestAmount()
    {
        const int balance = 100;
        const int paymentAmount = 1;
        _accountDataStore.GetAccount(Arg.Any<string>()).Returns(new Account{AllowedPaymentSchemes = AllowedPaymentSchemes.Bacs, Balance = balance});

        var result = _sut.MakePayment(new MakePaymentRequest{PaymentScheme = PaymentScheme.Bacs, Amount = paymentAmount});
        
        const int expectedAccountUpdateAmount = balance - paymentAmount;
        _accountDataStore.Received(1).UpdateAccount(Arg.Is<Account>(x=>x.Balance == expectedAccountUpdateAmount));
        result.Success.ShouldBeTrue();
    }
    
    [Test]
    [TestCase(AllowedPaymentSchemes.Bacs)]
    [TestCase(AllowedPaymentSchemes.FasterPayments)]
    public void MakePayment_ShouldNotUpdateAccount_WhenRequestPaymentSchemeIsChapsAndAccountAllowedPaymentSchemesDoesNotIncludeChaps(AllowedPaymentSchemes allowedPaymentScheme)
    {
        _accountDataStore.GetAccount(Arg.Any<string>()).Returns(new Account{AllowedPaymentSchemes = allowedPaymentScheme});

        var result = _sut.MakePayment(new MakePaymentRequest{PaymentScheme = PaymentScheme.Chaps});
        
        _accountDataStore.Received(0).UpdateAccount(Arg.Any<Account>());
        result.Success.ShouldBeFalse();
    }
    
    [Test]
    [TestCase(AccountStatus.Disabled)]
    [TestCase(AccountStatus.InboundPaymentsOnly)]
    public void MakePayment_ShouldNotUpdateAccount_WhenRequestPaymentSchemeIsChapsAndAccountAllowedPaymentSchemesIsChapsAndAccountIsNotLive(AccountStatus accountStatus)
    {
        _accountDataStore.GetAccount(Arg.Any<string>()).Returns(new Account{AllowedPaymentSchemes = AllowedPaymentSchemes.Chaps, Status = accountStatus, Balance = 100});

        var result = _sut.MakePayment(new MakePaymentRequest{PaymentScheme = PaymentScheme.Chaps, Amount = 1});
        
        _accountDataStore.Received(0).UpdateAccount(Arg.Any<Account>());
        result.Success.ShouldBeFalse();
    }
    
    [Test]
    public void MakePayment_ShouldUpdateAccount_WhenRequestPaymentSchemeIsChapsAndAccountAllowedPaymentSchemesIsChaps()
    {
        const int balance = 100;
        const int paymentAmount = 1;
        _accountDataStore.GetAccount(Arg.Any<string>()).Returns(new Account{AllowedPaymentSchemes = AllowedPaymentSchemes.Chaps, Balance = balance});

        var result = _sut.MakePayment(new MakePaymentRequest{PaymentScheme = PaymentScheme.Chaps, Amount = paymentAmount});
        
        const int expectedAccountUpdateAmount = balance - paymentAmount;
        _accountDataStore.Received(1).UpdateAccount(Arg.Is<Account>(x => x.Balance == expectedAccountUpdateAmount));
        result.Success.ShouldBeTrue();
    }
    
    [Test]
    public void MakePayment_ShouldUpdateAccount_WhenUpdateAccountThrowsAnException()
    {
        _accountDataStore
            .When(x => x.UpdateAccount(Arg.Any<Account>()))
            .Do(x => throw new Exception());
        _accountDataStore.GetAccount(Arg.Any<string>()).Returns(new Account{AllowedPaymentSchemes = AllowedPaymentSchemes.Chaps, Balance = 100});

        var result = _sut.MakePayment(new MakePaymentRequest{PaymentScheme = PaymentScheme.Chaps, Amount = 1});
        
        result.Success.ShouldBeFalse();
    }
}
