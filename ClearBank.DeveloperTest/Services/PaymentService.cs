﻿using System;
using ClearBank.DeveloperTest.Data;
using ClearBank.DeveloperTest.Types;
using System.Configuration;

namespace ClearBank.DeveloperTest.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly IAccountDataStore _dataStore;
        private readonly PaymentRequestValidator _paymentRequestValidator;

        public PaymentService(IAccountDataStore dataStore, PaymentRequestValidator paymentRequestValidator)
        {
            _dataStore = dataStore;
            _paymentRequestValidator = paymentRequestValidator;
        }

        public MakePaymentResult MakePayment(MakePaymentRequest request)
        {
            var account = _dataStore.GetAccount(request.DebtorAccountNumber);
            if (account == null)
            {
                return MakePaymentResult.Failed;
            }

            if (!_paymentRequestValidator.IsValidRequest(account, request))
            {
                return MakePaymentResult.Failed;
            }

            try
            {
                account.Balance -= request.Amount;
                _dataStore.UpdateAccount(account);
            }
            catch
            {
                return MakePaymentResult.Failed;
            }

            return MakePaymentResult.Succeeded;
        }
    }
}
