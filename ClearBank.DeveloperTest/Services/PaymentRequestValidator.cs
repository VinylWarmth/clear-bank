using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Services;

public class PaymentRequestValidator
{
    public bool IsValidRequest(Account account, MakePaymentRequest request)
    {
        switch (request.PaymentScheme)
        {
            case PaymentScheme.Bacs:
                if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Bacs))
                {
                    return false;
                }
                break;

            case PaymentScheme.FasterPayments:
                if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.FasterPayments))
                {
                    return false;
                }
                if (account.Balance < request.Amount)
                {
                    return false;
                }
                break;

            case PaymentScheme.Chaps:
                if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Chaps))
                {
                    return false;
                }
                if (account.Status != AccountStatus.Live)
                {
                    return false;
                }
                break;
            
        }

        return true;
    }
}
