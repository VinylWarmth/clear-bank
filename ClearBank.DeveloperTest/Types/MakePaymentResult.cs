﻿namespace ClearBank.DeveloperTest.Types
{
    public class MakePaymentResult
    {
        public bool Success { get; private set; }

        private MakePaymentResult() {}

        public static MakePaymentResult Succeeded => new() { Success = true };
        public static MakePaymentResult Failed => new() { Success = false };
    }
}
